package com.videomakerandmerging.sample.entity

class Effect(
    val id: Int,
    val name: String,
    val color: String,
    val effect: String
) {
    var startTime = 0
}