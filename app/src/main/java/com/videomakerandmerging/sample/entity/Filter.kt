package com.videomakerandmerging.sample.entity

class Filter(
  val name: String,
  val thumbnail: String,
  val config: String,
  val lut: String
)