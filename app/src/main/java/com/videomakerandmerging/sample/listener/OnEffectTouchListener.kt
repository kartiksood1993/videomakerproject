package com.videomakerandmerging.sample.listener

import com.videomakerandmerging.sample.entity.Effect

interface OnEffectTouchListener {

  fun onEffectTouchEvent(event: Int, effect: Effect)
}