package com.videomakerandmerging.sample.editor

enum class EditorPage {
    NONE,
    FILTER,
    AUDIO_MIX,
    OVERLAY,
    SUBTITLE,
    MV,
    SOUND,
    FILTER_EFFECT,
    TIME,
    TRANSITION,
    PAINT,
    COVER,
    FONT
}