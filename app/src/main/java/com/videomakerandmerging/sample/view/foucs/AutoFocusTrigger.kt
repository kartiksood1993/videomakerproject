package com.videomakerandmerging.sample.view.foucs

enum class AutoFocusTrigger {

    GESTURE,
    METHOD
}
