@file:Suppress("DEPRECATION")

package com.videomakerandmerging.sample

import android.annotation.SuppressLint
import android.graphics.Point
import android.os.Bundle
import android.util.TypedValue
import android.view.*
import android.view.GestureDetector.SimpleOnGestureListener
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.trinity.core.TrinityCore
import com.trinity.editor.MediaClip
import com.trinity.editor.TimeRange
import com.trinity.editor.TrinityVideoEditor
import com.trinity.listener.OnRenderListener
import com.videomakerandmerging.sample.editor.EffectController
import com.videomakerandmerging.sample.editor.EffectId
import com.videomakerandmerging.sample.editor.PlayerListener
import com.videomakerandmerging.sample.editor.ViewOperator
import com.videomakerandmerging.sample.entity.Effect
import com.videomakerandmerging.sample.entity.EffectInfo
import com.videomakerandmerging.sample.entity.MediaItem
import com.videomakerandmerging.sample.listener.OnEffectTouchListener
import com.videomakerandmerging.sample.view.*
import java.util.*

class EditorActivity : AppCompatActivity(), ViewOperator.AnimatorListener,
    ThumbLineBar.OnBarSeekListener,
    PlayerListener, OnEffectTouchListener, OnRenderListener {
    companion object {
        private const val USE_ANIMATION_REMAIN_TIME = 300 * 1000
    }

    private lateinit var mSurfaceContainer: FrameLayout
    private lateinit var mSurfaceView: SurfaceView
    private lateinit var mPasterContainer: FrameLayout
    private lateinit var mBottomLinear: HorizontalScrollView
    private lateinit var mPlayImage: ImageView
    private lateinit var mPauseImage: ImageView
    private lateinit var mActionBar: RelativeLayout
    private lateinit var mThumbLineBar: OverlayThumbLineBar
    private lateinit var mInsideBottomSheet: FrameLayout
    private lateinit var mBottomSheetLayout: CoordinatorLayout
    private lateinit var mVideoEditor: TrinityVideoEditor
    private var mUseInvert = false
    private var mCanAddAnimation = true
    private var mThumbLineOverlayView: ThumbLineOverlay.ThumbLineOverlayView? = null
    private lateinit var mEffectController: EffectController

    private val mEffects = LinkedList<EffectInfo>()
    private var mStartTime: Long = 0
    private val mActionIds = mutableMapOf<String, Int>()
    private var mFilterId = -1
    private var mVideoDuration = 0L
    private var mCurrentEditEffect: PasteUISimpleImpl? = null

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_editor)
        mVideoEditor = TrinityCore.createEditor(this)
        mEffectController = EffectController(this, mVideoEditor)
        mSurfaceContainer = findViewById(R.id.surface_container)
        mSurfaceView = findViewById(R.id.surface_view)
        val widthPixels = resources.displayMetrics.widthPixels
        val params = mSurfaceView.layoutParams
        params.width = widthPixels
        params.height = widthPixels * 16 / 9
        mSurfaceView.layoutParams = params
        mPasterContainer = findViewById(R.id.paster_view)
        val gesture = GestureDetector(this, mOnGestureListener)
        mPasterContainer.setOnTouchListener { _, event -> gesture.onTouchEvent(event) }
        mBottomLinear = findViewById(R.id.editor_bottom_tab)
        mPlayImage = findViewById(R.id.play)
        mPauseImage = findViewById(R.id.pause)
        mActionBar = findViewById(R.id.action_bar)
        mActionBar.setBackgroundDrawable(null)
        mThumbLineBar = findViewById(R.id.thumb_line_bar)

        mInsideBottomSheet = findViewById(R.id.frame_container)
        mBottomSheetLayout = findViewById(R.id.editor_coordinator)

        mPlayImage.setOnClickListener { mVideoEditor.resume() }
        mPauseImage.setOnClickListener { mVideoEditor.pause() }

        mVideoEditor.setSurfaceView(mSurfaceView)
        mVideoEditor.setOnRenderListener(this)
        val mediasArrays = intent.getSerializableExtra("medias") as Array<*>

        val medias = mutableListOf<MediaItem>()
        mediasArrays.forEach {
            val media = it as MediaItem
            val clip = MediaClip(media.path, TimeRange(0, media.duration.toLong()))
            mVideoEditor.insertClip(clip)
            mVideoDuration += media.duration
            medias.add(media)
        }
        val result = mVideoEditor.play(true)
        if (result != 0) {
            Toast.makeText(this, "Playback Failed $result", Toast.LENGTH_SHORT).show()
        }

        initThumbLineBar(medias)
    }

    private fun initThumbLineBar(medias: MutableList<MediaItem>) {
        if (medias.isEmpty()) {
            return
        }
        val thumbnailSize =
            TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32f, resources.displayMetrics)
                .toInt()
        val thumbnailPoint = Point(thumbnailSize, thumbnailSize)
        val config = ThumbLineConfig.Builder()
            .screenWidth(windowManager.defaultDisplay.width)
            .thumbPoint(thumbnailPoint)
            .thumbnailCount(10)
            .build()
        mThumbLineOverlayView = object : ThumbLineOverlay.ThumbLineOverlayView {
            val rootView =
                LayoutInflater.from(applicationContext).inflate(R.layout.timeline_overlay, null)

            override fun getContainer(): ViewGroup {
                return rootView as ViewGroup
            }

            override fun getHeadView(): View {
                return rootView.findViewById(R.id.head_view)
            }

            override fun getTailView(): View {
                return rootView.findViewById(R.id.tail_view)
            }

            override fun getMiddleView(): View {
                return rootView.findViewById(R.id.middle_view)
            }

        }
        mThumbLineBar.setup(medias, config, this, this)
        mEffectController.setThumbLineBar(mThumbLineBar)
    }

    private fun changePlayResource() {

    }

    override fun onThumbLineBarSeek(duration: Long) {
        mThumbLineBar.pause()
        changePlayResource()
        mCanAddAnimation = if (mUseInvert) {
            duration > EditorActivity.Companion.USE_ANIMATION_REMAIN_TIME
        } else {
            mVideoEditor.getVideoDuration() - duration < USE_ANIMATION_REMAIN_TIME
        }
        mVideoEditor.seek(duration.toInt())
    }

    override fun onThumbLineBarSeekFinish(duration: Long) {
        mThumbLineBar.pause()
        changePlayResource()
        mCanAddAnimation = if (mUseInvert) {
            duration > USE_ANIMATION_REMAIN_TIME
        } else {
            mVideoEditor.getVideoDuration() - duration < USE_ANIMATION_REMAIN_TIME
        }
    }

    override fun getCurrentDuration(): Long {
        return mVideoEditor.getCurrentPosition()
    }

    override fun getDuration(): Long {
        return mVideoEditor.getVideoDuration()
    }

    override fun updateDuration(duration: Long) {
    }

    override fun onEffectTouchEvent(event: Int, effect: Effect) {
        val effectLocalDir = externalCacheDir?.absolutePath

        if (event == MotionEvent.ACTION_DOWN) {
            mStartTime = mVideoEditor.getCurrentPosition()
            mVideoEditor.resume()
            if (effect.id == EffectId.UNDO.ordinal) {
                return
            } else {
                val actionId = mVideoEditor.addAction(effectLocalDir + "/" + effect.effect)
                mActionIds[effect.name] = actionId
            }
            effect.startTime = mStartTime.toInt()
            mEffectController.onEventAnimationFilterLongClick(effect)
        } else if (event == MotionEvent.ACTION_UP) {
            mVideoEditor.pause()
            if (effect.id == EffectId.UNDO.ordinal) {
                if (!mEffects.isEmpty()) {
                    val info = mEffects.removeLast()
                    mEffectController.onEventAnimationFilterDelete(Effect(0, "", "", ""))
                    mVideoEditor.deleteAction(info.actionId)
                    mVideoEditor.seek(info.startTime.toInt())
                }
                return
            } else {
                val endTime = mVideoEditor.getCurrentPosition()
                val effectInfo = EffectInfo()
                val actionId = mActionIds[effect.name] ?: return
                effectInfo.actionId = actionId
                effectInfo.startTime = mStartTime
                effectInfo.endTime = endTime
                mEffects.add(effectInfo)

                // 删除同一时间的特效,保留当前的
                mEffects.forEach {
                    if (mStartTime >= it.startTime && endTime <= it.endTime && actionId != it.actionId) {
                        mVideoEditor.deleteAction(it.actionId)
                    }
                }

                mVideoEditor.updateAction(mStartTime.toInt(), endTime.toInt(), actionId)
                mEffectController.onEventAnimationFilterClickUp(effect)
            }
        }
    }


    override fun onShowAnimationEnd() {
    }

    override fun onHideAnimationEnd() {
        mVideoEditor.resume()
    }

    override fun onPause() {
        super.onPause()
        mVideoEditor.pause()
    }

    override fun onResume() {
        super.onResume()
        mVideoEditor.resume()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mFilterId != -1) {
            mVideoEditor.deleteFilter(mFilterId)
        }
        mActionIds.forEach {
            mVideoEditor.deleteAction(it.value)
        }
        mActionIds.clear()
        mVideoEditor.destroy()
    }

    private val mOnGestureListener = object : SimpleOnGestureListener() {

        private var mPosX = 0F
        private var mPosY = 0F
        private var mShouldDrag = false

        fun shouldDrag(): Boolean {
            return mShouldDrag
        }

        override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
            mVideoEditor.pause()
            mCurrentEditEffect?.showTextEdit(false)
            return mShouldDrag
        }

        override fun onScroll(
            e1: MotionEvent,
            e2: MotionEvent,
            distanceX: Float,
            distanceY: Float
        ): Boolean {
            if (shouldDrag()) {
                if (mPosX == 0F || mPosY == 0F) {
                    mPosX = e1.x
                    mPosY = e1.y
                }
                val x = e2.x
                val y = e2.y
                mCurrentEditEffect?.moveContent(x - mPosX, y - mPosY)
                mPosX = x
                mPosY = y
            }
            return mShouldDrag
        }

        override fun onFling(
            e1: MotionEvent?,
            e2: MotionEvent?,
            velocityX: Float,
            velocityY: Float
        ): Boolean {
            return mShouldDrag
        }

        override fun onDown(e: MotionEvent): Boolean {
            if (mCurrentEditEffect?.isPasteRemoved() == true) {
                mCurrentEditEffect = null
            }
            mShouldDrag = if (mCurrentEditEffect != null) {
                mCurrentEditEffect?.isEditCompleted() == false &&
                        mCurrentEditEffect?.contentContains(e.x, e.y) ?: false &&
                        mCurrentEditEffect?.isVisibleInTime(getCurrentDuration()) ?: false
            } else {
                false
            }
            mPosX = 0F
            mPosY = 0F
            return mShouldDrag
        }
    }

    override fun onSurfaceCreated() {
    }

    override fun onDrawFrame(textureId: Int, width: Int, height: Int, matrix: FloatArray?): Int {
        return -1
    }

    override fun onSurfaceDestroy() {
    }

}
